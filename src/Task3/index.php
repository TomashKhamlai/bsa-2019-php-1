<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\FightArena;
use App\Task3\FightArenaHtmlPresenter;
use App\Task1\Fighter;

$fighters = [
    [
        'id' => 1,
        'name' => 'Ryu',
        'health' => 100,
        'attack' => 10,
        'image' => 'https://bit.ly/2E5Pouh',
    ],
    [
        'id' => 2,
        'name' => 'Chun-Li',
        'health' => 70,
        'attack' => 30,
        'image' => 'https://bit.ly/2Vie3lf'
    ],
    [
        'id' => 3,
        'name' => 'Ken Masters',
        'health' => 80,
        'attack' => 20,
        'image' => 'https://bit.ly/2VZ2tQd'
    ],
];

$arena = new FightArena();

foreach ($fighters as $fighter) {
    extract($fighter);
    $fighter = new Fighter($id, $name, $health, $attack, $image);
    $arena->add($fighter);
}


$presenter = new FightArenaHtmlPresenter();
$presentation = $presenter->present($arena);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
</head>
<body>
    <?php echo $presentation; ?>
</body>
</html>
