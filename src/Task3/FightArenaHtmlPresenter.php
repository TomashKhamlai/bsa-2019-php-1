<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

/**
 * FightArenaHtmlPresenter
 */
class FightArenaHtmlPresenter
{
    /**
     * @param FightArena $arena
     * @return string
     */
    public function present(FightArena $arena): string
    {
        $images = [];
        $spans = [];

        foreach ($arena->all() as $fighter) {
            $spans[] = $this->implodeInSpan(
                $this->getMainInfo($fighter),
                $this->getImageTag($fighter->getImage()),
                ' '
            );
        }

        $spans = implode($spans);


        return $spans;
    }

    /**
     * @param string $src
     * @return string
     */
    private function getImageTag(string $src): string
    {
        return "<img src=\"$src\"></span>";
    }

    /**
     * @param Fighter $fighter
     * @return string
     */
    private function getMainInfo(Fighter $fighter): string
    {
        return "{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}";
    }

    /**
     * @param string $info
     * @param string $img
     * @param string $glue
     * @return string
     */
    private function implodeInSpan(string $info, string $img, string $glue)
    {
        return "<span>{$info}{$glue}{$img}</span>";
    }
}
