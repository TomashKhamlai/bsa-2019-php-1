<?php

declare(strict_types=1);

namespace App\Task1;

/**
 * Fight arena
 */
class FightArena
{
    /**
     * @var array|Fighter[]
     */
    private $fighters = [];

    /**
     * @param Fighter $fighter
     *
     * @return void
     */
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    /**
     * @return Fighter|null
     */
    public function mostPowerful(): ?Fighter
    {
        if (!$this->fighters) {
            return null;
        }

        $power = 0;

        $mostPowerful = null;

        foreach ($this->fighters as $fighter) {
            if ($fighter->getAttack() > $power) {
                $mostPowerful = $fighter;
            }
        }

        return $mostPowerful;
    }

    /**
     * @return Fighter|null
     */
    public function mostHealthy(): ?Fighter
    {
        if (!$this->fighters) {
            return null;
        }

        $scores = 0;

        $mostHealthy = null;

        foreach ($this->fighters as $fighter) {
            if ($fighter->getHealth() > $scores) {
                $scores = $fighter->getHealth();
                $mostHealthy = $fighter;
            }
        }

        return $mostHealthy;
    }

    /**
     * @return array|Fighter[]
     */
    public function all(): array
    {
        return $this->fighters;
    }
}
